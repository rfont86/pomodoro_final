const NO_TIME_LEFT = 0

class Pomodoro {
  constructor(bus, time) {
    this.timeLeft = NO_TIME_LEFT
    this.time = time
    this.bus = bus

    this._subscribe()
  }

  _subscribe() {
    this.bus.subscribe('timer.start_requested', this.start.bind(this))
    this.bus.subscribe('timer.askTimeLeft', this.calculateTimeLeft.bind(this))
    this.bus.subscribe('timer.pause', this.pause.bind(this))
    this.bus.subscribe('timer.reset', this.reset.bind(this))
  }

  start() {
    this.bus.publish('timer.start', this._message())
  }

  pause() {
    this.bus.publish('timer.paused', this._message())
  }

  reset() {
    this.bus.publish('timer.reseted', this._message())
  }

  calculateTimeLeft() {
    this.bus.publish('timer.timeLeft', this._message())
  }

  _message() {
    return { minutes: this.timeLeft, isPaused: this.isPaused }
  }

  _hasTimeLeft() {
    return (this.timeLeft > NO_TIME_LEFT)
  }
}

export default Pomodoro
