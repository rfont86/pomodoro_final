import Time from '../../js/libraries/Time.js'
import StubDate from '../StubDate.js'

describe('Time', () => {
  it('gives the current moment in milliseconds', () => {
    const aSecond = 1000
    const date = new StubDate(aSecond)
    const time = new Time(date)

    const currentMoment = time.currentMoment()

    expect(currentMoment).toEqual(aSecond)
  })

  xit('transforms milliseconds to minutes', () => {
    const milliseconds = 120000
    const minutes = 2
    const time = new Time()

    const result = time.toMinutes(milliseconds)

    expect(result).toEqual(minutes)
  })

  xit('transforms milliseconds to minutes rounding up', () => {
    const milliseconds = 119999
    const minutes = 2
    const time = new Time()

    const result = time.toMinutes(milliseconds)

    expect(result).toEqual(minutes)
  })
})
